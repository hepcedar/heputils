## Makefile for HEPUtils

VERSION := 1.4.3alpha
CXX := g++ -std=c++14
CXXFLAGS := -O2

include Makefile.inc


.PHONY = install uninstall dist clean distclean all pyext

all: pyext
	@true
#$(warning No default make target: use 'make install' to install HEPUtils headers)

check:
	$(CXX) -I./include ./tests/EtaTest.cc -o etaTest && ./etaTest
	$(CXX) -I./include ./tests/PtTest.cc -o ptTest && ./ptTest
	$(CXX) -I./include ./tests/RapidityTest.cc -o rapidityTest&& ./rapidityTest
	$(CXX) -I./include ./tests/ThetaTest.cc -o thetaTest && ./thetaTest
	$(CXX) -I./include ./tests/PhaseSpaceTestIntacos.cc -o intacosTest && ./intacosTest
	$(CXX) -I./include ./tests/VectorDot3Test.cc -o vectorDot3Test && ./vectorDot3Test
	$(CXX) -I./include ./tests/VectorAngleTest.cc -o vectorAngleTest && ./vectorAngleTest
	$(CXX) -I./include ./tests/VectorSetPMTest.cc -o vectorSetPMTest && ./vectorSetPMTest

pyext: heputils.i
	if (which swig > /dev/null); then \
	    swig -python -c++ -cpperraswarn -Iinclude heputils.i && \
	    $(CXX) $(CXXFLAGS) -shared -fPIC heputils_wrap.cxx -o _heputils.so -I./include $(FASTJET_CPPFLAGS) \
	      `python-config --includes` $(FASTJET_LIBFLAGS); \
	    else echo "swig is not available: not building Python wrapper" 1>&2; fi

install:
	mkdir -p $(PREFIX)/include
	cp -r include/* $(PREFIX)/include/
	test -e _heputils.so && \
	    PYV=`python --version 2>&1 | sed 's/Python \(.*\)\..*/python\1/'` && \
	    PYDIR=$(PREFIX)/lib/$$PYV/site-packages && mkdir -p $$PYDIR && \
	    cp heputils.py _heputils.so $$PYDIR

uninstall:
	rm -rf $(PREFIX)/include/HEPUtils
	rm -rf $(PREFIX)/lib*/heputils.py
	rm -rf $(PREFIX)/lib*/_heputils.so

dist:
	tar czf HEPUtils-$(VERSION).tar.gz README TODO ChangeLog Makefile include heputils.i

clean:
	rm -f heputils.py heputils_wrap.cxx _heputils.so
	rm -f *Test

distclean: clean
	rm -f HEPUtils-$(VERSION).tar.gz
