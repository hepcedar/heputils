#include "HEPUtils/Vectors.h"
#include "TestTools.h"


// double azimuthal(HEPUtils::P4 vec) {
//     if (vec.p2() == 0) {
//         return 0;
//     }
//     else return acos(vec.pz()/vec.p());
// }

double azimuthal(const HEPUtils::P4& vec) {
  if (vec.p2() == 0) return 0;
  if (vec.pz() == 0) return 0.5 * M_PI;
  return atan2(vec.rho(),vec.pz());
}


int main() {

  const double lower_bound=-100000, upper_bound=100000;
  std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
  std::default_random_engine generator(time(0));

  double dot3_product, dot3_manual;
  double p1x, p1y, p1z, m1;
  HEPUtils::P4 vec, vec_sus;

  vec_sus = vec_sus.setPM(0., 0., 0., 1.);

  if (!almost_equal(vec_sus.theta(), azimuthal(vec_sus), 100)) {
    return 1;
  }

  for (int i=0; i<10000; i++) {
    p1x = unif(generator);
    p1y = unif(generator);
    p1z = unif(generator);
    m1 = unif(generator);


    if (m1 < 0) {
      i--;
      continue;
    }

    vec = vec.setPM(p1x, p1y, p1z, m1);

    if (!almost_equal(vec.theta(), azimuthal(vec), 1000)) {
      return 1;
    }
  }
  return 0;
}
