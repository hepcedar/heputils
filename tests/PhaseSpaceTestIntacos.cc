#include "HEPUtils/PhaseSpace.h"
#include "TestTools.h"


double intacos( double x) {
  return x * acos(x) - sqrt(1 - x*x);
}


/// @todo What is the point of this test?!?
int main() {

  double lower_bound=-1, upper_bound=1;
  std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
  std::default_random_engine generator(time(0));

  double x;
  for (int i=0; i < 100; i++) {
    x = unif(generator);
    if (!almost_equal(HEPUtils::iacos(x), intacos(x), 1)) {
      return 1;
    }
  }

  return 0;
}
